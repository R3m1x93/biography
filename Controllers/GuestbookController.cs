﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Guestbook.Controllers
{
    public class GuestbookController : Controller
    {
        //
        // GET: /Guestbook/
        private Guestbook.Models.GuestbookContext _db=new Guestbook.Models.GuestbookContext();
        public ActionResult Index()
        {
            var mostRecentEntries=(from entry in _db.Entries orderby entry.DateAdded descending select entry).Take(20);
            ViewBag.Entries = mostRecentEntries.ToList();
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Guestbook.Models.GuestbookEntry entry)
        {
            entry.DateAdded = DateTime.Now;
            _db.Entries.Add(entry);
            _db.SaveChanges();
            return RedirectToAction("Index");

        }

    }
}
